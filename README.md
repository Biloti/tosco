## Welcome ##

**ToSCo** is a set of scientific computing programs, mainly for processing geophysical data.

This project is associated to GêBR project. More about of GêBR is available at [GêBR Projet site](http://www.gebrproject.com).

This project hosts programs which can be used from GêBR, taking care of porting and easily back distributing them to the community. That is a way to expand GêBR's abilities. Anyone can contribute programs to ToSCo.
