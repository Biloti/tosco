#!/bin/bash

#   Copyright 2009-2020 Ricardo Biloti <biloti@unicamp.br>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

function usage ()
{
CWPROOT="$STOW_PATH/su"

echo "SU install made easy (4th ed) - A cortesy of GeBR Project
Syntax: $0 [options]

Options:
    -h,-? : show this help
       -d : SU GIT URL ($SU_GIT_URL)
       -o : Download path ($DOWNLOAD_PATH)
       -u : Uninstall SU ($UNINSTALL)
       -U : Keep previously installed SU ($KEEP_OLDER_SUS)
       -T : Target path to install SU ($TARGET_PATH)
       -F : Force reinstall ($REINSTALL)
       -t : Text mode (do not use graphical client to ask
            for passwords)

SU install path: $CWPROOT

This scripts is designed to Ubuntu/Debian Linux systems.
Debian users however should have administration privileges
through sudo utility. See the post
http://sl.gebrproject.com/easy-su-install

The newest version of this script is on
http://download.gebrproject.com/pub/scripts/gebr-su-install.sh

Any problem with this script, please report to
Ricardo Biloti <biloti@unicamp.br>

2009-2021 (c) Ricardo Biloti - GeBR Project
http://www.gebrproject.com/
"
}

function check_pkg {

    dpkg -l $1 | grep ^ii > /dev/null 2>&1
    if [ $? -eq 1 ]; then
	echo "missing. Scheduling it for installing"
	PKGS_TO_INSTALL="$PKGS_TO_INSTALL $1"
    else
	echo "present"
    fi
}

# Default values
#------------------------------------------------------------------------------#

DOWNLOAD_PATH="$HOME/Downloads"
SU_GIT_URL="https://github.com/JohnWStockwellJr/SeisUnix.git"
UNINSTALL="FALSE"
KEEP_OLDER_SUS="FALSE"
REINSTALL="FALSE"
TEXT_MODE="-A"
TARGET_PATH="/usr/local"
STOW_PATH="$TARGET_PATH/stow"
SU_VERSION="44R19"

# Parsing command line parameters
#------------------------------------------------------------------------------#
while getopts "T:V:o:d:uUFth" OPT; do
  case $OPT in
      "h") usage; exit 0               ;;
      "?") usage; exit 0               ;;
      "o") DOWNLOAD_PATH="$OPTARG"     ;;
      "d") SU_GIT_URL="$OPTARG"       ;;
      "u") UNINSTALL="TRUE"            ;;
      "U") KEEP_OLDER_SUS="TRUE"       ;;
      "F") REINSTALL="TRUE"            ;;
      "t") TEXT_MODE=""                ;;
      "T") TARGET_PATH="$OPTARG";STOW_PATH="$TARGET_PATH/stow";;
  esac
done

# Try to become root before start
if [ $EUID != 0 ]; then
    SUDO_ASKPASS=${SUDO_ASKPASS:-"/usr/lib/openssh/gnome-ssh-askpass"} sudo $TEXT_MODE $0 $@ 2> /dev/null || echo "Please, try run $0 as root."
    exit
fi

export CWPROOT="$STOW_PATH/su"

cat <<EOF
SU install made easy - A cortesy of the GeBR Project

Try "$0 -h" to see the usage guide.
EOF

# Uninstall SU and exits
if [ "$UNINSTALL" == 'TRUE' ]; then
    cd $CWPROOT/..
    if [ -d su ]; then
	echo "Uninstalling SU"
	stow -D su
    else
	echo "Nothing to uninstall"
    fi
    exit 0;
fi

# Checks whether this intended version is already (partially) installed
if [ -d "$CWPROOT/bin" -a "$REINSTALL" == "FALSE" ]; then 
    echo "
It seems that SU is already installed or, at least,
partially installed. This install attempt has been aborted now.

To reinstall SU, overwriting such previous install,
check the reinstall option of this script.

See the help with \"$0 -h\"."
    exit 0
else
    echo -e "
It seems that SU is already installed or, at least,
partially installed. Proceeding anyway, as requested.\n"
fi
    
if [ ! -d "$CWPROOT" ]; then
    echo "Creating CWPROOT dir....... $CWPROOT"
    mkdir -m 755 -p "$CWPROOT"
fi

if [ ! -d "$DOWNLOAD_PATH" ]; then
    mkdir -p "$DOWNLOAD_PATH"
fi

# Clone or update GIT repo
cd "$DOWNLOAD_PATH"
echo -n "SU source tree............. "
cd "$DOWNLOAD_PATH"
if [ -d "SeisUnix/.git" ]; then
    cd SeisUnix
    git pull -q "$SU_GIT_URL"
    git checkout -q -- '*'
    echo "updated"
else
    git clone -q "$SU_GIT_URL"
    echo "cloned"
    cd SeisUnix
fi

echo "Testing for required packages"

PKGS_TO_INSTALL=""

echo -n "gcc........................ "
check_pkg gcc

echo -n "gfortran................... "
check_pkg gfortran

echo -n "stow....................... "
check_pkg stow

echo -n "motif  .................... "
check_pkg libmotif-dev

echo -n "GLUT....................... "
check_pkg freeglut3-dev

echo -n "Xmu........................ "
check_pkg libxmu-dev

if [ "$PKGS_TO_INSTALL"'x' != 'x' ]; then
    echo "Installing missing packages"
    apt -y install $PKGS_TO_INSTALL
fi

echo "Compiling..."

rsync -av --delete "$DOWNLOAD_PATH/SeisUnix/" "$CWPROOT"
cd "$CWPROOT/src"

# Adapt Makefile.config
cat >Makefile.config.patch <<EOF
15a16
> CWPROOT=$CWPROOT
49c50
< XDRFLAG = -DSUXDR
---
> XDRFLAG =
94c95
< OPTC = -g -std=c90  -m64 -Wall -ansi  -Wno-long-long 
---
> OPTC = -g -O3 -m64 -Wno-long-long
100c101
< FFLAGS = \$(FOPTS) -ffixed-line-length-none  -fallow-argument-mismatch
---
> FFLAGS = \$(FOPTS) -ffixed-line-length-none
EOF
cp Makefile.config Makefile.config-original
patch -p2 Makefile.config Makefile.config.patch

touch LICENSE_"$SU_VERSION"_ACCEPTED
touch MAILHOME_"$SU_VERSION"

if [ ! -e chkroot.sh-original ]; then
   cp chkroot.sh chkroot.sh-original
fi
if [ ! -e license.sh-original ]; then
   cp license.sh license.sh-original
fi
if [ ! -e mailhome.sh-original ]; then
   cp mailhome.sh mailhome.sh-original
fi
cat chkroot.sh-original | sed 's/read RESP/RESP="y"/' > /tmp/chkroot.sh
cat mailhome.sh-original | sed 's/read RESP/RESP="n"/' > /tmp/mailhome.sh
cat license.sh-original | sed 's/read RESP/RESP="y"/;s/more/cat/' > /tmp/license.sh
chmod 755 /tmp/chkroot.sh /tmp/license.sh /tmp/mailhome.sh
mv /tmp/chkroot.sh /tmp/license.sh /tmp/mailhome.sh .

# Patch to Cshot to produces traces up to 4001 samples
cd Fortran/Cshot

cat > cshot2.f.patch <<EOF
100c100
<      :            MAXTPT =1001,
---
>      :            MAXTPT =4001,
EOF

cat > graphics.f.patch <<EOF
63c63
<       PARAMETER ( NXPTS = 10)
---
>       PARAMETER ( NXPTS = 20)
EOF

cp cshot2.f cshot2.f-original
patch -p2 cshot2.f cshot2.f.patch
cp graphics.f graphics.f-original
patch -p2 graphics.f graphics.f.patch

echo "Compiling SU package"

# Clean de sujeiras que não deveriam estar no repositório
cd "$CWPROOT/src/su/include" && make clean
cd "$CWPROOT/src/su/src/Fortran/Cwell" && make clean

cd "$CWPROOT/src"
for target in install xtinstall finstall \
    mglinstall utils xminstall; do
    CWPROOT="$CWPROOT" colormake $target
done

echo -e "\nCompilation done."
cd $CWPROOT/..

stow -D su
stow -v su

if [ $EUID != 0 ]; then
    PROFILE="$HOME/.profile"
    BASHRC="$HOME/.bashrc"
else 
    PROFILE="/etc/profile"
    BASHRC="/etc/bash.bashrc"
fi

sed '/CWPROOT=/d' $PROFILE > /tmp/profile
echo "export CWPROOT=$CWPROOT" >> /tmp/profile
cp /tmp/profile $PROFILE

echo "export CWPROOT=$CWPROOT" > /tmp/bash.bashrc
sed '/CWPROOT=/d' $BASHRC >> /tmp/bash.bashrc
cp /tmp/bash.bashrc $BASHRC

echo "Installation done."
echo -e "SU will be available next time you log in.\n"
echo "Any problem with this script, please report to"
echo "Ricardo Biloti <biloti@unicamp.br>."
